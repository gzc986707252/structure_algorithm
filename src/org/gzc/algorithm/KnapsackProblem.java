package org.gzc.algorithm;

/**
 * @Description: 背包问题 -- 动态规划
 * @Author: guozongchao
 * @Date: 2020/8/19 20:41
 */
public class KnapsackProblem {
    public static void main(String[] args) {
        int[] w = {1, 4, 3};//物品的重量
        int[] val = {1500, 3000, 2000}; //物品的价值
        int m = 4; //背包的容量
        int n = val.length; //物品的个数
        //创建二维数组，
        // v[i][j] 表示在前 i 个物品中能够装入容量为 j 的背包中的最大价值
        int[][] v = new int[n + 1][m + 1];

        int[][] path = new int[n + 1][m + 1];

        //初始化第一行和第一列, 这里在本程序中，可以不去处理，因为默认就是 0

        //从第二行开始，也就是从第一个物品开始 ，检查是否满足背包容量为j的最大价值
        for (int i = 1; i < v.length; i++) {
            for (int j = 1; j < v[i].length; j++) {  //从第二列开始
                if (w[i - 1] > j) {  //如果当前物品的重量比当前背包容量大，就说明装不下了，保留原来背包的物品，即规划表的上一个单元格
                    v[i][j] = v[i - 1][j];
                } else {
                    //否则 ，需要权衡当前背包的物品的价值 是否比 当前物品价值+剩余容量能装下的物品的最大价值(前面已解决的小问题) 哪个价值更大
//                    v[i][j] = Math.max(v[i - 1][j], val[i - 1]+v[i-1][j - w[i - 1]]);

                    //为了记录放入的哪些物品
                    if (v[i - 1][j] < val[i - 1] + v[i - 1][j - w[i - 1]]) {
                        v[i][j] = val[i - 1] + v[i - 1][j - w[i - 1]];
                        path[i][j] = 1;
                    } else {
                        v[i][j] = v[i - 1][j];
                    }
                }
            }
        }


        //打印动态规划表
        for (int i = 0; i < v.length; i++) {
            for (int j = 0; j < v[i].length; j++) {
                System.out.print(v[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println();
        //打印path[][]
        for (int i = 0; i < path.length; i++) {
            for (int j = 0; j < path[i].length; j++) {
                System.out.print(path[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println();

        int i = path.length - 1; //行的最大下标
        int j = path[0].length - 1; //列的最大下标
        while (i > 0 && j > 0) {
            //从 path 的最后开始找
            if (path[i][j] == 1) {
                System.out.printf("第%d 个商品放入到背包\n", i);
                j -= w[i - 1];
            }
            i--;
        }
    }


}
