package org.gzc.tree;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @Description: 哈夫曼编码测试
 * @Author: guozongchao
 * @Date: 2020/8/14 15:24
 */
public class Test {
    public static void main(String[] args) {
        HuffmanCode huffmanCode = new HuffmanCode();
//        System.out.println(Integer.parseInt("10101111", 2));
//        System.out.println((byte)175);
        String content = "i like like like java do you like a java";
        System.out.println("原始的数据：" + content);
        byte[] bytes = content.getBytes();
        System.out.println("未编码字节数组：" + Arrays.toString(bytes));
        for (byte b : bytes) {
            System.out.print(Integer.toBinaryString(b) + "  ");
        }
        System.out.println();

        byte[] encodeBytes = huffmanCode.encode(bytes);
        System.out.println("已编码字节数组：" + Arrays.toString(encodeBytes));
        for (byte b : encodeBytes) {
            System.out.print(Integer.toBinaryString(b) + "  ");
        }
        System.out.println();

        final byte[] decodeBytes = huffmanCode.decode(encodeBytes);
        System.out.println("解码后的字节数组：" + Arrays.toString(decodeBytes));

        System.out.println("还原后的数据：" + new String(decodeBytes));

    }


}
