package org.gzc.linklist;

import java.util.LinkedList;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/8/2 20:10
 */
public class Main {
    public static void main(String[] args) {

        //测试数据
        StudentNode node1 = new StudentNode(1, "张三");
        StudentNode node2 = new StudentNode(5, "李四");
        StudentNode node3 = new StudentNode(3, "王五");
        StudentNode node4 = new StudentNode(12, "赵六");
        StudentNode node5 = new StudentNode(9, "马云");
        StudentNode node6 = new StudentNode(11, "lalalalalal");

        //创建带头结点的链表
        SingleLinkList linkList1 = new SingleLinkList();

        //添加元素(没有排序)
        linkList1.add(node1);
        linkList1.add(node2);
        linkList1.add(node3);
        linkList1.add(node4);
        linkList1.add(node5);

        //未排序结点遍历
        System.out.println("未排序结点遍历:");
        linkList1.display();

        //插入一个结点
        System.out.println("\n再添加一个编号为10结点后：");
        linkList1.add(node6);
        linkList1.display();
/*
       //获取一个指定结点
        StudentNode studentNode1 = linkList1.get(5);
        System.out.println("\n获取5号的结点" + studentNode1);
        StudentNode studentNode2 = linkList1.get(10);
        System.out.println("获取10号的结点" + studentNode2);

        System.out.println("\n修改：");
        try {
            linkList1.update(5, "lisi");
            linkList1.update(10,"gzc");
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }

        System.out.println("\n删除：");
        try {
            linkList1.delete(5);
            linkList1.delete(10);
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }finally {
            linkList1.display();
        }
*/

        SingleLinkList linkList2 = new SingleLinkList();
        //排序插入元素
        linkList2.addByOrder(node1);
        linkList2.addByOrder(node2);
        linkList2.addByOrder(node3);
        linkList2.addByOrder(node4);
        linkList2.addByOrder(node5);
        //排序结点遍历
        System.out.println("\n排序后结点遍历:");
        linkList2.display();

        //插入一个结点
        System.out.println("\n再添加一个编号为10结点后：");
        linkList2.add(node6);
        linkList2.display();

    }
}


