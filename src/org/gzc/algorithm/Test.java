package org.gzc.algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/8/23 0:35
 */
public class Test {
    public static void main(String[] args) {
        int[] list = new int[]{2,5,8,9,7,6};
        Arrays.sort(list);
        List<Integer> i = new ArrayList<>();
        i.sort((o1, o2) -> o1 - o2);
    }
}
