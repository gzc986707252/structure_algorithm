package org.gzc.linklist;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/8/3 11:11
 */ //学生结点
public class StudentNode {
    public int no;
    public String name;
    public StudentNode next;

    public StudentNode(int no, String name) {
        this.no = no;
        this.name = name;
    }

    @Override
    public String toString() {
        return "StudentNode{" +
                "no=" + no +
                ", name='" + name +
                "'}";
    }
}
