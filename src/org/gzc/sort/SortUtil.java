package org.gzc.sort;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/8/7 23:38
 */
public class SortUtil {
    private SortStrategy sortStrategy;

    public SortUtil(SortStrategy sortStrategy) {
        this.sortStrategy = sortStrategy;
    }

    public void sort(int[] array) {
        sortStrategy.sort(array);
    }
}
