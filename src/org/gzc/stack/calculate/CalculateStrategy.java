package org.gzc.stack.calculate;

/**
 * @Description:  计算方式策略接口
 * @Author: guozongchao
 * @Date: 2020/8/5 13:56
 */
public interface CalculateStrategy {
    double calculate(String expression);
}
