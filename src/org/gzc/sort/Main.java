package org.gzc.sort;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * @Description: 测试
 * @Author: guozongchao
 * @Date: 2020/8/7 23:37
 */
public class Main {
    public static void main(String[] args) {

//        int[] array = new int[]{87, 28, 226, 139, 296, 91, 364, 664, 221, 586, 384};
        int[] array = new int[8000000];
        for (int i = 0; i < 8000000; i++) {
            array[i] = (int) (Math.random() * 800000);
        }

        SortUtil sortUtil = new SortUtil(new HeapSort());
        long startTimeMills = System.currentTimeMillis();
        sortUtil.sort(array);
//        Arrays.sort(array);
        long endTimeMills = System.currentTimeMillis();
//        System.out.println("最终结果："+ Arrays.toString(array));
        System.out.println("排序执行时间：" + (endTimeMills - startTimeMills) + "毫秒");

    }
}
