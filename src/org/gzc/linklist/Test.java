package org.gzc.linklist;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/8/24 22:52
 */
public class Test {
    static class ListNode {
        int val;
        ListNode next = null;

        ListNode(int val) {
            this.val = val;
        }
    }
    public static void main(String[] args) {
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        ListNode node5 = new ListNode(5);
        ListNode node6 = new ListNode(6);
        node1.next=node2;
        node2.next=node3;
        node3.next=node5;
        //node5.next=node6;

        node4.next = node6;

        ListNode node = FindFirstCommonNode(node1, node4);
        System.out.println(node.val);

    }

    public static ListNode FindFirstCommonNode(ListNode pHead1, ListNode pHead2) {
        if(pHead1==null || pHead2==null){
            return null;
        }
        ListNode p1=pHead1;
        ListNode p2=pHead2;
        while(p1!=p2){
            p1 = p1.next==null?pHead2:p1.next;
            p2 = p2.next ==null?pHead1:p2.next;
        }
        return p1;
    }
}
