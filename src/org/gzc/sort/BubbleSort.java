package org.gzc.sort;


/**
 * @Description: 冒泡排序
 * 时间复杂度 O(n^2) ,空间复杂度 O(1)
 * @Author: guozongchao
 * @Date: 2020/8/7 22:23
 */
public class BubbleSort implements SortStrategy {
    @Override
    public void sort(int[] array) {
        int temp;
        boolean flag=false;   //标识符，用于优化，如果前面已经排好序了，后面就没有必要在进行比较排序了
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - 1-i; j++) {
                if (array[j] > array[j + 1]) {
                    flag=true;
                    temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
            if(!flag){  //如果在一趟排序中，都没有执行交换，那就说明已将是排好序了，后面就不必要在执行了
                break;
            }
            //System.out.println("第"+(i+1)+"趟排序后：");
            //System.out.println(Arrays.toString(array));
        }
    }
}
