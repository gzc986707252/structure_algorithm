package org.gzc.search;

import java.util.List;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/8/11 12:11
 */
public interface SearchStrategy {
    int searchOne(int[] array, int key);

    List<Integer> searchMore(int[] array, int key);
}
