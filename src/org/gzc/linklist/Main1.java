package org.gzc.linklist;

/**
 * @Description: 输入一个链表，输出该链表中倒数第k个结点。
 * @Author: guozongchao
 * @Date: 2020/8/1 13:19
 */
public class Main1 {

    public static void main(String[] args) {

        ListNode n1=new ListNode(1);
        ListNode n2=new ListNode(2);
        ListNode n3=new ListNode(3);
        ListNode n4=new ListNode(4);
        ListNode n5=new ListNode(5);
        ListNode n6=new ListNode(6);
        n1.next=n2;
        n2.next=n3;
        n3.next=n4;
        n4.next=n5;
        n5.next=n6;

        System.out.println("初始链表：");
        show(n1);
        System.out.println("\n倒数第二个结点");
        ListNode listNode = FindKthToTail(n1, 10);
        show(listNode);

    }

    public static ListNode FindKthToTail(ListNode head,int k) {
        if(head==null||k<=0)
            return null;

        //遍历链表获得结点总个数
        ListNode temp=head;
        int count=0;
        while(temp!=null){
            count++;
            temp=temp.next;
        }
        if(k>count)
            return null;
        temp=head;
        for(int i=0;i<count-k;i++){
            temp=temp.next;
        }
        return temp;
    }

    public static void show(ListNode listNode) {
        ListNode temp=listNode;
        while (temp != null) {
            System.out.printf("--> %d\t", temp.val);
            temp=temp.next;
        }
    }
}
class ListNode {
    int val;
    ListNode next = null;

    ListNode(int val) {
        this.val = val;
    }
}