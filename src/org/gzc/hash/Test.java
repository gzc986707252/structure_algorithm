package org.gzc.hash;

import java.util.Scanner;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/8/13 12:20
 */
public class Test {
    public static void main(String[] args) {
//        Emp emp1 = new Emp(154, "jack", "北京");
//        Emp emp2 = new Emp(153, "张三", "上海");
//        Emp emp3 = new Emp(134, "mary", "纽约");
//        Emp emp4 = new Emp(155, "李四", "广州");
//        Emp emp5 = new Emp(164, "老王", "杭州");
//        Emp emp6 = new Emp(145, "John", "深圳");
        HashTab hashTab = new HashTab(6);
        Scanner scanner = new Scanner(System.in);
        while(true){
            System.out.println("add(添加)、get(获取)、show(显示)、exit(退出)");
            System.out.print("-->>");
            String option = scanner.next();
            switch (option) {
                case "add":
                    System.out.print("请输入id:");
                    int id = scanner.nextInt();
                    System.out.print("请输入姓名：");
                    String name = scanner.next();
                    System.out.print("请输入地址：");
                    String address = scanner.next();
                    Emp emp = new Emp(id, name, address);
                    hashTab.put(id, emp);
                    break;
                case "get":
                    System.out.print("输入id:");
                    int id1=scanner.nextInt();
                    Emp emp1 = hashTab.get(id1);
                    System.out.println(emp1);
                    break;
                case "show":
                    System.out.println("哈希表的存储情况如下：");
                    hashTab.display();
                    break;
                case "exit":
                    scanner.close();
                    System.exit(-1);
                default:
                    break;
            }

        }

    }
}
