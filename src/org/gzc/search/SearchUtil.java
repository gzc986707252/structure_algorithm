package org.gzc.search;

import java.util.List;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/8/11 12:24
 */
public class SearchUtil implements SearchStrategy {
    private SearchStrategy searchStrategy;

    public SearchUtil(SearchStrategy searchStrategy) {
        this.searchStrategy=searchStrategy;
    }
    @Override
    public int searchOne(int[] array, int key) {
        return searchStrategy.searchOne(array, key);
    }

    @Override
    public List<Integer> searchMore(int[] array, int key) {
        return searchStrategy.searchMore(array, key);
    }
}
