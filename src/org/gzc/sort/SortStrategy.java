package org.gzc.sort;

/**
 * @Description: 排序策略的接口
 * @Author: guozongchao
 * @Date: 2020/8/7 22:21
 */
public interface SortStrategy {
    void sort(int[] array);
}
