package org.gzc.search;

import java.util.List;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/8/11 12:27
 */
public class Test {
    public static void main(String[] args) {
        int[] array = new int[]{28, 87, 91, 139, 221, 226,226,226, 296, 364,364, 384, 586, 664};
        int key=87;
        SearchUtil searchUtil = new SearchUtil(new InsertValueSearch());
        int i = searchUtil.searchOne(array, key);
        System.out.println(i==-1?"查询无果":(key+"的索引值为"+i));

        //List<Integer> results = searchUtil.searchMore(array, 226);
        //System.out.println("查询结果："+results);

    }
}
