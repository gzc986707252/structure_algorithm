package org.gzc.stack.calculate;

/**
 * @Description: 测试用例
 * @Author: guozongchao
 * @Date: 2020/8/5 17:47
 */
public class Test {
    public static void main(String[] args) {
        String exp1 = "1+2+3";  //6
        String exp2 = "5*4+6-4/2";   //24
        String exp3 = "5*(4/2+3)+4-1";  //28
        String exp4 = "5*(6+(4*3-4)/2)-3+5";  //52
        String exp5 = "5 . 5*(6. 5 +(4  * 3. 8-4.1 ) / 2.5)-3.5+5.6";  //62.27

       // Calculator calculator = new Calculator();  //默认策略计算表达式

        //采用中缀表达式转换成后缀表达式的方式计算
        Calculator calculator = new Calculator(new SuffixExpressionCalculateStrategy());
        System.out.println(calculator.calculate(exp1));
        System.out.println(calculator.calculate(exp2));
        System.out.println(calculator.calculate(exp3));
        System.out.println(calculator.calculate(exp4));
        System.out.println(calculator.calculate(exp5));
    }
}
