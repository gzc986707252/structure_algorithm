package org.gzc.queue;

import com.sun.javafx.sg.prism.web.NGWebView;
import sun.misc.Cleaner;

import java.io.Console;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @Description:
 * @Author: guozongchao
 * @Date: 2020/8/1 23:51
 */
public class Main {
    public static void main(String[] args) {
        //测试一把
        //创建一个队列
        //ArrayQueue queue = new ArrayQueue(4);
        //CircleArrayQueue queue = new CircleArrayQueue(4);
        CircleArrayQueue2<Integer> queue = new CircleArrayQueue2<>(5);
        char key = ' '; //接收用户输入
        Scanner scanner = new Scanner(System.in);
        boolean loop = true;
        //输出一个菜单
        while(loop) {
            System.out.println("s(show): 显示队列");
            System.out.println("e(exit): 退出程序");
            System.out.println("a(add): 添加数据到队列");
            System.out.println("g(get): 从队列取出数据");
            System.out.println("h(head): 查看队列头的数据");
            key = scanner.next().charAt(0);//接收一个字符
            switch (key) {
                case 's':
                    queue.show();
                    break;
                case 'a':
                    System.out.println("输出一个数");
                    int value = scanner.nextInt();
                    try {
                        queue.add(value);
                    } catch (Exception e) {
                        // TODO: handle exception
                        System.out.println(e.getMessage());
                    }
                    break;
                case 'g': //取出数据
                    try {
                        int res = queue.get();
                        System.out.printf("取出的数据是%d\n", res);
                    } catch (Exception e) {
                        // TODO: handle exception
                        System.out.println(e.getMessage());
                    }
                    break;
                case 'h': //查看队列头的数据
                    try {
                        int res = queue.getHeadItem();
                        System.out.printf("队列头的数据是%d\n", res);
                    } catch (Exception e) {
                        // TODO: handle exception
                        System.out.println(e.getMessage());
                    }
                    break;
                case 'e': //退出
                    scanner.close();
                    loop = false;
                    break;
                default:
                    break;
            }
        }
        System.out.println("程序退出~~");
    }
}
