package org.gzc.stack.calculate;

/**
 * @Description: 计算器类,采用策略模式将不同的计算方式封装起来
 * @Author: guozongchao
 * @Date: 2020/8/5 13:42
 */
public class Calculator {
    private CalculateStrategy calculateStrategy;

    public Calculator() {
        this.calculateStrategy = new DefaultCalculateStrategy();
    }
    public Calculator(CalculateStrategy strategy) {
        this.calculateStrategy=strategy;
    }

    public double calculate(String expression) {
        return calculateStrategy.calculate(expression);
    }

}
